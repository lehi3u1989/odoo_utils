# -*- coding: utf-8 -*-

import re
import os
import logging
import locale
from pathlib import Path
from contextlib import contextmanager
from os.path import exists as file_exists, join
from distutils.version import LooseVersion

from sh import pew as pew_cmd, Command  # @UnresolvedImport
from pew import pew
from pew._utils import env_bin_dir, temp_environ
from pew.pew import workon_home, unsetenv, mkvirtualenv


#
# Helpers command to manage virtual environments with PEW
#


def setup(name, project=None, requirements=[], packages=[],
          linked_env=[], linked_dir=[], symlink_dir=[], depth=1):
    """
    Create or update an existing virtual envs
    """

    locale.setlocale(locale.LC_CTYPE, 'en_US.UTF-8')

    project = project and Path(project) or None
    links = linked_env and get_env_paths(name, linked_env) or []
    links += linked_dir or []

    if not exists(name):
        logging.warning('Create %s virtual environment...', name)
        mkvirtualenv(name, project=project)

    logging.warning('Update %s virtual environment...', name)
    update(name, requirements, packages, links=links, symlinks=symlink_dir)
    logging.warning('%s virtual environment is ready', name)


def update(name, requirements=[], packages=[], links=[], symlinks=[]):
    """
    Update an existing virtual environment, optionally linked with another
    """

    def log_out(line):
        line = line.strip()
        logging.debug(line)

    def log_err(line):
        line = line.strip()
        logging.error(line)

    env_path = str(get_paths(name).absolute())

    if symlinks:
        for symlink in symlinks:
            filename = re.search('(?P<name>[^\/]+)$', symlink).group('name')
            target_link = join(env_path, filename)
            if not file_exists(target_link):
                logging.warning('Symlink from %s to %s', symlink, target_link)
                os.symlink(symlink, target_link)

    with in_env(name):
        for link in links:
            logging.warning(
                'Link %s virtual environment with path %s', name, link)
            pew_cmd.add([link],
                        _out=log_out, _err=log_err, _env=os.environ)

    with in_env(name) as pip:
        # Downgrade pip version in virtualenv
        pip.install(['-U', 'pip==7.1.2'])
        combined_req = combine_requirements(name, requirements)
        reqs = ['-r', combined_req]
        if reqs or packages:
            logging.warning(
                'Install packages for %s virtual environment', name)
            pip.install(['--allow-all-external'] + reqs + packages,
                        _out=log_out, _err=log_err, _env=os.environ)
        else:
            logging.warning('No project specific requirements.txt found.')


def exists(name):
    """
    Check if a virtual environment already exists
    """
    return name in pew.lsenvs()


def get_env_paths(target_env, linked_env):
    """
    Retrieve all non existing python lib path in the target env
    """
    paths = []
    for env in linked_env:
        linked_path = str(get_paths(env).absolute())
        current_path = str(get_paths(target_env).absolute())
        links = ''
        links_path = join(current_path, '_virtualenv_path_extensions.pth')

        if file_exists(links_path):
            with open(links_path, 'r') as links_file:
                links = links_file.read()

        if not re.search(linked_path, links):
            paths.append(linked_path)

    return paths


def get_paths(name):
    """
    Get all python package paths from a virtual environment
    """
    with in_env(name):
        paths = pew.sitepackages_dir()

    return paths


@contextmanager
def in_env(name):
    """
    Run command in a temporary virtual env context
    """
    with temp_environ():
        envdir = workon_home / name
        os.environ['VIRTUAL_ENV'] = str(envdir)
        os.environ['PATH'] = os.pathsep.join([
            str(envdir / env_bin_dir),
            os.environ['PATH'],
        ])
        unsetenv('PYTHONHOME')
        unsetenv('__PYVENV_LAUNCHER__')

        pip_bin = join(str(envdir / env_bin_dir), 'pip')
        pip = None
        if file_exists(pip_bin):
            pip = Command(pip_bin)

        yield pip


def check_valid_version(ver1, ver2):
    """
    check version of 2 required library
    """
    if ver1[1] is None:
        return ver2
    if ver2[1] is None:
        return ver1
    v1 = LooseVersion(ver1[1])
    op1 = ver1[0]
    v2 = LooseVersion(ver2[1])
    op2 = ver2[0]
    if (v1 == v2 and (op1, op2) in [
            ('==', '=='), ('>=', '<='), ('<=', '>=')]) or (
            v1 > v2 and (op1, op2) == ('>=', '>=')) or (
            v1 < v2 and (op1, op2) == ('<=', '<=')):
        return ver1
    if (v1 < v2 and (op1, op2) == ('>=', '>=')) or (
            v1 > v2 and (op1, op2) == ('<=', '<=')):
        return ver2
    return False


def combine_requirements(name, work_dir):
    """
    Combine all requirements.txt files to one file
    """

    TMP_DEST = '/tmp/%s_requirements.txt' % name
    PATTERN = "(?P<lib_name>[a-z0-9A-Z\-\_]+)((>|<|=)=)?(.*)"
    PATTERN_HTTP = "^(https?://|git\+).*"
    exclude_path = ''
    if 'odoo' not in name:
        exclude_path = '-not -path "%s/odoo/*"' % work_dir
    fcmd = 'find %s %s -name requirements.txt ' % (work_dir, exclude_path) +\
           '-exec cat "{}" \; -exec echo "" \; > %s' % TMP_DEST
    regex_lib = re.compile(PATTERN)
    regex_http = re.compile(PATTERN_HTTP)
    os.popen(fcmd)
    f_src = open(TMP_DEST, 'r')
    tmp_list = {}
    for line in f_src.readlines():
        match = regex_http.match(line)
        if match:
            tmp_list[line] = (None, None)
            continue
        match = regex_lib.match(line)
        if match:
            lib_name = match.group('lib_name').lower().strip() or None
            lib_ver = match.group(4) or None
            if lib_name is None:
                continue
            if lib_name not in tmp_list:
                tmp_list[lib_name] = (match.group(2), lib_ver)
            else:
                ver = check_valid_version(
                    tmp_list[lib_name], (match.group(2), lib_ver))
                if ver is False:
                    logging.warning('[module conflict] %s: %s%s and %s%s' % (
                        lib_name, tmp_list[lib_name][0],
                        tmp_list[lib_name][1], match.group(2), lib_ver))
                else:
                    tmp_list[lib_name] = ver
    f_src.close()
    f_dst = open(TMP_DEST, 'w+')
    for lib in tmp_list:
        if tmp_list[lib][1] is not None and tmp_list[lib][0] is not None:
            f_dst.writelines(lib + '%s%s\n' % tmp_list[lib])
        else:
            f_dst.writelines(lib + '\n')
    f_dst.close()

    return TMP_DEST
