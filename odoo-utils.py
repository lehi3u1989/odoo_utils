#!/usr/bin/python

import os
import subprocess
import sys
import logging
import click
import yaml
import virtual_env
from virtual_env import in_env

from pathlib import Path

from os.path import join, isdir, abspath, basename, exists
from sh import pew as pew_cmd, Command  # @UnresolvedImport
from pew import pew
from pew._utils import env_bin_dir, temp_environ
from pew.pew import workon_home, unsetenv, mkvirtualenv


def get_work_dir():
    """
    Return the absolute path to project root folder
    """
    current_dir = abspath(os.getcwd())
    work_dir = abspath(join(current_dir, os.pardir))
    return work_dir


def get_config_file_path(environment, work_dir):
    config_path = abspath(join(work_dir, 'config'))
    config_file = environment + '.conf'
    return config_path + '/' + config_file


def get_addons_path(work_dir):
    """
    get a list of addons-path
    """

    odoo_dir = join(work_dir, 'OCB')
    extra_addons = join(work_dir, 'extra_addons')
    project_dir = join(work_dir, 'project')

    addons = []

    # Odoo addons path
    if isdir(odoo_dir):
        if isdir(join(odoo_dir, 'openerp', 'addons')):
            addons.append('OCB/openerp/addons')
        if isdir(join(odoo_dir, 'addons')):
            addons.append('OCB/addons')

    # extra addons path
    if isdir(extra_addons):
        for name in os.listdir(extra_addons):
            if isdir(join(extra_addons, name)):
                addons.append(join('extra_addons', name))

    # project specific addons path
    if isdir(project_dir):
        addons.append('project')

    exec_path = os.getcwd()
    addons = [abspath(join(exec_path, work_dir, addon)) for addon in addons]

    return ','.join(addons)


def get_project_config_yml(work_dir):
    """
    Return the config info in /config/project_config.yml
    """
    config_path = abspath(join(work_dir, 'config'))
    return config_path + '/' + 'project_config.yml'


def setup_virtual_env(work_dir, offline=False):
    """
    Ensure all virtual env are ready
    References:
        - https://pypi.python.org/pypi/virtualenv
        - https://pypi.python.org/pypi/pew/
        - https://github.com/berdario/pew#usage
        - https://security.openstack.org/guidelines/dg_avoid-dangerous-input-parsing-libraries.html
    """
    if offline:
        logging.debug('Offline mode, skip checking virtual env.')
        return

    # folder to store and share virtuallenvs is
    # /opt/openerp/.local/share/virtualenvs

    # load project_config.yml
    project_config_yml_file = get_project_config_yml(work_dir)
    project_config_yml = {}
    if exists(project_config_yml_file):
        with open(project_config_yml_file, 'r') as config_file:
            project_config_yml = yaml.load(config_file) or {}

    if not project_config_yml:
        logging.error(
            'Please update file project_config.yml! Abort!')
        sys.exit(1)

    # fix openoffice uno dependency issue
    symlink_dir = ['/usr/lib/python2.7/dist-packages/uno.py',
                   '/usr/lib/python2.7/dist-packages/unohelper.py']
    symlink_dir = [f for f in symlink_dir if exists(f)]

    # setup virtual env for odoo
    # get odoo version: 'odoo-9'
    odoo_env = 'odoo-%s' % project_config_yml['project']['odoo_version']
    odoo_dir = join(work_dir, 'OCB')
    virtual_env.setup(odoo_env,
                      requirements=odoo_dir,
                      packages=['pyopenssl', 'ndg-httpsclient', 'pyasn1'],
                      symlink_dir=symlink_dir,
                      depth=0)

    # setup virtual env for project
    # get project name based on the node folder of workdir: 'demo90',
    project_env = basename(work_dir)
    virtual_env.setup(project_env,
                      project=work_dir,
                      requirements=work_dir,
                      packages=['pyopenssl', 'ndg-httpsclient', 'pyasn1'],
                      linked_env=[odoo_env])


def run_odoo(work_dir, config_file_path, addons_path, options):
    """
    Call OCB/openerp-server to run Odoo server with options
    """
    # setup virtual_env
    setup_virtual_env(work_dir, offline=False)
    virtual_env = basename(work_dir)

    # run openerp-server
    # command = "%s --config %s --addons-path %s %s $@" % (openerp_server_file_path, config_file_path, addons_path, options)
    openerp_server_file_path = work_dir + '/OCB/openerp-server'

    command = [
        openerp_server_file_path,
        '--config', config_file_path,
        '--addons-path', addons_path
    ]

    if options:
        command.append(options)

    with in_env(virtual_env):
        subprocess.call(command)

    return True


@click.group()
@click.option('--debug/--no-debug', default=False)
def odoo(debug):
    """
    This is used to call sub functions: start, upgrade, ...
    """
    click.echo('Debug mode is %s' % ('on' if debug else 'off'))
    return True


@odoo.command()
@click.option('-e', '--environment', default='local', help='Odoo environments: local, integration, staging, production.')
@click.option('-o', '--options', default=False, help='Additional options: --test ...')
def start(environment, options):
    """
    Start Odoo instance.
    """
    logging.warning('===== starting Odoo ...')

    # get work directory
    work_dir = get_work_dir()

    # get config path
    config_file_path = get_config_file_path(environment, work_dir)

    # get addons path
    addons_path = get_addons_path(work_dir)

    # run openerp-server
    run_odoo(work_dir, config_file_path, addons_path, options)

    return True


@odoo.command()
@click.option('-e', '--environment', default='local', help='Odoo environments: local, integration, staging, production.')
@click.option('-d', '--database', default=False, help='Database in use.')
@click.option('-m', '--modules', default=False, help='Modules need to be upgrade: comma-separated list, use \"all\" for all modules')
@click.option('-o', '--options', default=False, help='Additional options.')
def install(environment, database, modules, options):
    """
    Install modules in a database.
    """
    if not database:
        logging.error(
            'Please pass a database name in argument! Abort!')
        sys.exit(1)

    if not modules:
        logging.error(
            'Please pass a list of modules separated by comma in argument! '
            'Abort!')
        sys.exit(1)

    # get work directory
    work_dir = get_work_dir()

    # get config path
    config_file_path = get_config_file_path(environment, work_dir)

    # get addons path
    addons_path = get_addons_path(work_dir)

    # run openerp-server with options to upgrade modules

    options = options or ''
    options += ' --database %s --init %s' % (database, modules)
    run_odoo(work_dir, config_file_path, addons_path, options)


@odoo.command()
@click.option('-e', '--environment', default='local', help='Odoo environments: local, integration, staging, production.')
@click.option('-d', '--database', default=False, help='Database in use.')
@click.option('-m', '--modules', default=False, help='Modules need to be upgrade: comma-separated list, use \"all\" for all modules')
@click.option('-o', '--options', default=False, help='Additional options.')
def upgrade(environment, database, modules, options):
    """
    Upgrade modules in a database.
    """
    if not database:
        logging.error(
            'Please pass a database name in argument! Abort!')
        sys.exit(1)

    if not modules:
        logging.error(
            'Please pass a list of modules separated by comma in argument! Abort!')
        sys.exit(1)

    # get work directory
    work_dir = get_work_dir()

    # get config path
    config_file_path = get_config_file_path(environment, work_dir)

    # get addons path
    addons_path = get_addons_path(work_dir)

    # run openerp-server with options to upgrade modules

    options = options or ''
    options += ' --database %s --update %s' % (database, modules)
    run_odoo(work_dir, config_file_path, addons_path, options)


if __name__ == '__main__':
    """
    init function odoo
    """
    odoo()
